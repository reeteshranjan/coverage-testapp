from django.shortcuts import render

from django.http import HttpResponse

from core.models import *

# Create your views here.
def test_api(request):

    location = Location(title = 'My Test Location', latitude = '10.1311141', longitute = '75.0114141')

    business = Business.create(location = location, title = 'My Business')

    buyer = Buyer.objects.create(first_name = 'Reetesh', last_name = 'Ranjan')

    return HttpResponse('All well!')
