source ~/.virtualenvs/coverage/bin/activate
sudo -u postgres psql -c "drop database coverage;"
sudo -u postgres createdb --username coverage --host 127.0.0.1 coverage
./manage.py migrate
cqlsh -u coverage -p coverage -e "drop keyspace coverage"
./manage.py syncdb --database cassandra
deactivate
