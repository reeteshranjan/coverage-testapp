#!/bin/bash
#
# ------------------------------------------------------------------------------
# Summary: Script for setup of coverage test app
# ------------------------------------------------------------------------------


# ------------------------------------------------------------------------------
# FUNCTIONS
# ------------------------------------------------------------------------------

# Print an error message
function error_message {
    echo " "
    echo "[ERROR] "$1
    echo " "
    echo "Coverage Test App setup failed!"
}

# Print help for the setup script
function print_help {
    echo " "
    echo "./setup.sh [OPTIONS]"
    echo " "
    echo "  -h|--help           print this help"
    echo "  -f|--force          force setup steps discarding the prorgess made so far"
    echo "  -v|--verbose        verbose output for debugging"
}

# ------------------------------------------------------------------------------
# MAIN SCRIPT
# ------------------------------------------------------------------------------

# Parse command line parameters

OPT_VERBOSITY=0

OPT_FORCE_SETUP=0

for arg in "$@"
do
    case $arg in
        -v|--verbose)
            OPT_VERBOSITY=1
            shift
            ;;
        -f|--force)
            OPT_FORCE_SETUP=1
            shift # past argument=value
            ;;
        -h|--help)
            print_help
            exit 0
            ;;
        *)
            error_message "Unknown command line argument: $arg"
            exit 1
            ;;
    esac
done

# Directory that remembers setup progress

SETUP_DIR=.setup

mkdir -p $SETUP_DIR

if [ 1 -eq $OPT_FORCE_SETUP ]
then
    # forget all setup progress history
    rm -f $SETUP_DIR/*.*
fi

export LC_ALL="en_US.UTF-8"
export LC_CTYPE="en_US.UTF-8"

mkdir -p log

# Requirements checks
if [ ! -f $SETUP_DIR/requirements_checks_pass ]
then
    #
    # 1. Check available memory
    #
    REQUIRED_MEM=`expr 1000 \* 1000` # in KBs - required memory is 1GB

    available_mem=`grep MemTotal /proc/meminfo | awk '{print $2}'`

    if [ $available_mem -lt $REQUIRED_MEM ]
    then
        error_message "Your system needs to have at least 1GB RAM to install Coverage Test App"
        exit 1
    fi

    #
    # 2. Check system/environment - it has to be Ubuntu 14.04.xx LTS
    #
    if [ ! -f /etc/issue.net -o "$(grep 'Ubuntu 14.04.*LTS' /etc/issue.net)" = "" ]
    then
        error_message "You must be on an Ubuntu 14.04.XX LTS system to install Coverage Test App"
        exit 1
    fi

    touch $SETUP_DIR/requirements_checks_pass
fi

echo "------------------------------------------------------------"
echo "[PASS] System requirements checks"
echo "------------------------------------------------------------"

if [ ! -f $SETUP_DIR/apt_deps_installed ]
then
    #
    # Update the system when we first setup
    #
    sudo apt-get update && sudo apt-get upgrade

    if [ "0" != "$?" ]
    then
        error_message "Updating the system failed"
        exit 1
    fi

    #
    # Install all apt packages required at once
    #
    sudo apt-get install -y postgresql postgresql-contrib python-pip python-dev libpq-dev build-essential libev4 libev-dev

    if [ "0" != "$?" ]
    then
        error_message "Installing apt dependencies failed"
        exit 1
    fi

    touch $SETUP_DIR/apt_deps_installed
fi

echo "------------------------------------------------------------"
echo "[PASS] apt dependencies installed (other than cassandra)"
echo "------------------------------------------------------------"

if [ ! -f $SETUP_DIR/cassandra_installed ]
then
    #
    # Install dependencies
    #

    if [ ! -f $SETUP_DIR/cassandra_deps_configured ]
    then
        sudo add-apt-repository -y ppa:webupd8team/java && sudo apt-get update

        if [ "0" != "$?" ]
        then
            error_message "Installing cassandra dependencies failed"
            exit 1
        fi

        touch $SETUP_DIR/cassandra_deps_configured
    fi

    if [ ! -f $SETUP_DIR/cassandra_deps_installed ]
    then
        sudo apt-get install -y oracle-java8-installer

        if [ "0" != "$?" ]
        then
            error_message "Installing cassandra dependencies failed"
            exit 1
        fi

        touch $SETUP_DIR/cassandra_deps_installed
    fi

    if [ ! -f $SETUP_DIR/cassandra_repo_configured ]
    then
        echo "deb http://debian.datastax.com/community stable main" | sudo tee -a /etc/apt/sources.list.d/cassandra.sources.list

        curl -L https://debian.datastax.com/debian/repo_key | sudo apt-key add -

        if [ "0" != "$?" ]
        then
            error_message "Configuring cassandra dependencies repositories failed"
            exit 1
        fi

        touch $SETUP_DIR/cassandra_repo_configured
    fi

    sudo apt-get update && sudo apt-get install -y dsc22 cassandra=2.2.6 cassandra-tools=2.2.6

    if [ "0" != "$?" ]
    then
        error_message "Installing cassandra failed"
        exit 1
    fi

    sudo service cassandra stop && sudo rm -rf /var/lib/cassandra/data/system/*

    touch $SETUP_DIR/cassandra_installed
fi

echo "------------------------------------------------------------"
echo "[PASS] Cassandra installed"
echo "------------------------------------------------------------"

if [ ! -f $SETUP_DIR/python_packages_installed ]
then
    #
    # Install all pip dependencies
    #
    sudo pip install virtualenv virtualenvwrapper

    if [ "0" != "$?" ]
    then
        error_message "Installing pip dependencies failed"
        exit 1
    fi

    touch $SETUP_DIR/python_packages_installed
fi

echo "------------------------------------------------------------"
echo "[PASS] Virtual environment pip packages installed"
echo "------------------------------------------------------------"

if [ ! -f $SETUP_DIR/virtualenv_created ]
then
    #
    # Add things needed in ~/.bashrc as well as run them for now
    #
    if [ ! -f $SETUP_DIR/virtualenv_profile_setup ]
    then
        echo "source /usr/local/bin/virtualenvwrapper.sh" >> ~/.bashrc
        echo "export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib" >> ~/.bashrc

        touch $SETUP_DIR/virtualenv_profile_setup
    fi

    #
    # Locale setup is important for virtual env wrapper
    #
    if [ ! -f $SETUP_DIR/locale_setup ]
    then

        head -n 1 /etc/environment > $SETUP_DIR/.environment

        echo "LC_ALL=en_US.UTF-8" >> $SETUP_DIR/.environment
        echo "LANG=en_US.UTF-8" >> $SETUP_DIR/.environment

        sudo cp $SETUP_DIR/.environment /etc/environment

        if [ "0" != "$?" ]
        then
            error_message "Changing environment failed"
            exit 1
        fi

        echo 'LANG="en_US.UTF-8"' > $SETUP_DIR/.default.locale

        sudo cp $SETUP_DIR/.default.locale /etc/default/locale

        if [ "0" != "$?" ]
        then
            error_message "Changing default locale failed"
            exit 1
        fi

        sudo locale-gen "en_US.UTF-8" && sudo dpkg-reconfigure locales

        if [ "0" != "$?" ]
        then
            error_message "Setting locale failed"
            exit 1
        fi

        touch $SETUP_DIR/locale_setup
    fi

    source /usr/local/bin/virtualenvwrapper.sh
    export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib

    #
    # Create virtual environment
    #
    mkvirtualenv coverage

    if [ "0" != "$?" ]
    then
        error_message "Creating virtual environment failed"
        exit 1
    fi

    touch $SETUP_DIR/virtualenv_created

else
    #
    # In case we are coming in after breaking the script somewhere, let's get into virtual env
    #

    source /usr/local/bin/virtualenvwrapper.sh
    export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib

    workon coverage
fi

echo "------------------------------------------------------------"
echo "[PASS] Virtual environment setup and entered "
echo "------------------------------------------------------------"

if [ ! -f $SETUP_DIR/virtualenv_requirements_installed ]
then
    #
    # Install all PIP deps
    #
    pip install -r ./requirements.txt

    if [ "0" != "$?" ]
    then
        error_message "Installing deps inside the virtual environment failed"
        exit 1
    fi

    touch $SETUP_DIR/virtualenv_requirements_installed
fi

echo "------------------------------------------------------------"
echo "[PASS] Project deps installed in virtual env "
echo "------------------------------------------------------------"

if [ ! -f $SETUP_DIR/postgres_db_setup ]
then

    #
    # Copy our pg_hba.conf that allows md5 logins
    #
    if [ ! -f $SETUP_DIR/postgres_setup_logins ]
    then

        sudo cp conf/postgresql/pg_hba.conf /etc/postgresql/9.3/main/pg_hba.conf

        if [ "0" != "$?" ]
        then
            error_message "Copying postgres db conf file failed"
            exit 1
        fi

        touch $SETUP_DIR/postgres_setup_logins

	sudo service postgresql restart
    fi

    #
    # Create the user and db as required
    #
    if [ ! -f $SETUP_DIR/postgres_db_user_created ]
    then

        echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
        echo ">>> Please use the password 'coverage' (without the single quotes) when prompted <<<"
        echo "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"

        sudo -u postgres createuser -Pse coverage

        if [ "0" != "$?" ]
        then
            error_message "Creating the postgres DB user failed"
            exit 1
        fi

        touch $SETUP_DIR/postgres_db_user_created
    fi

    #
    # Now create the DB according to the env
    #

    if [ ! -f $SETUP_DIR/postgres_db_created ]
    then
        echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
        echo ">>> Please use the password 'coverage' (without the single quotes) when prompted <<<"
        echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"

        sudo -u postgres createdb --username coverage --host 127.0.0.1 coverage

        if [ "0" != "$?" ]
        then
            error_message "Creating the postgres DB failed"
            exit 1
        fi

        touch $SETUP_DIR/postgres_db_created
    fi

    touch $SETUP_DIR/postgres_db_setup
fi

echo "------------------------------------------------------------"
echo "[PASS] Postgres user and db are setup"
echo "------------------------------------------------------------"

if [ ! -f $SETUP_DIR/cassandra_db_setup ]
then
    #
    # Setup the system_auth first
    #

    echo "Going to start cassandra and wait for it to boot up ..."

    sudo service cassandra start && sleep 20

    if [ ! -f $SETUP_DIR/cassandra_system_auth_setup ]
    then

        cqlsh -u cassandra -p cassandra -e "ALTER KEYSPACE system_auth WITH REPLICATION = { 'class' : 'SimpleStrategy', 'replication_factor' : 1 };"

        if [ "0" != "$?" ]
        then
            error_message "Setting up cassandra system auth failed"
            exit 1
        fi

        touch $SETUP_DIR/cassandra_system_auth_setup
    fi

    #
    # Copy our cassandra.yaml that allows PasswordAuthenticator
    #
    if [ ! -f $SETUP_DIR/cassandra_yaml_copied ]
    then

        sudo cp conf/cassandra/cassandra.yaml /etc/cassandra/ && sudo service cassandra stop && sudo service cassandra start

        if [ "0" != "$?" ]
        then
            error_message "Copying cassandra yaml failed"
            exit 1
        fi

        echo "Waiting for cassandra to restart ..."
        sleep 15

        touch $SETUP_DIR/cassandra_yaml_copied
    fi

    #
    # Create cassandra user for our project
    #
    if [ ! -f $SETUP_DIR/cassandra_user_created ]
    then

        cqlsh -u cassandra -p cassandra -e "CREATE USER coverage WITH PASSWORD 'coverage';"

        if [ "0" != "$?" ]
        then
            error_message "Setting up cassandra system auth failed"
            exit 1
        fi

        touch $SETUP_DIR/cassandra_user_created
    fi

    #
    # Create the DB now
    #

    python manage.py syncdb --database cassandra

    if [ "0" != "$?" ]
    then
        error_message "Creating the cassandra DB by migrating failed"
        exit 1
    fi

    touch $SETUP_DIR/cassandra_db_setup
fi

echo "------------------------------------------------------------"
echo "[PASS] Cassandra user and db are setup"
echo "------------------------------------------------------------"

./manage.py makemigrations && ./manage.py migrate

if [ "0" != "$?" ]
then
    error_message "Making and running latest migrations failed"
    exit 1
fi

echo "------------------------------------------------------------"
echo "[PASS] Migrations are completed. Start the server as follows"
echo "$ workon coverage"
echo "$ ./manage.py runserver"
echo "------------------------------------------------------------"
