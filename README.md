# Coverage.py Test App

Sample django app to debug [0% code coverage report issue](https://bitbucket.org/ned/coveragepy/issues/445/django-app-cannot-connect-to-cassandra).

## Setup
### Requirements
* Ubuntu 14.04.x TLS
* 1GB RAM

### Steps
```
$ ./setup.sh
```
Running this command sets up the virtual environment and all dependency packages.

#### Troubleshooting

Sometimes, Cassandra can produce an error failing the setup script; however, this issue is 'temporary'. If you run the setup.sh script again, this error goes away.

## App Summary

* Models - 1 Cassandra model, 1 Postgresql model
* Test URLs - /api/v1/testcov (creates objects of given models)

## Collecting Coverage Data

### Run Server with coverage.py

```
~/coverage-testapp$ workon coverage
(coverage) ~/coverage-testapp$ coverage run.py --source="." manage.py runserver
```

### Make Test API call
```
~/coverage-testapp$ curl "http://127.0.0.1:8000/api/v1/testcov"
All well!
```
### Create Coverage Reports

Kill the running server and

```
(coverage) ~/coverage-testapp$ coverage report
```

## Clear Databases

If you need to clean-up your database (as the test API inserts new records every run):
```
~/coverage-testapp$ ./reset-db.sh
```