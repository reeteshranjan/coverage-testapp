from __future__ import unicode_literals

# SQL
from django.db import models as sqlmodels
from django.db.models import Model as SQLModel

# NOSQL
from cassandra.cqlengine import columns
from cassandra.cqlengine.models import Model as NoSQLModel
from cassandra.cqlengine.usertype import UserType

import uuid

# Create your models here.

# Location is a Cassandra User Defined Type

class Location(UserType):
    title           = columns.Text(required = True)
    latitude        = columns.Decimal(required = True)
    longitude       = columns.Decimal(required = True)

# Business is a Cassandra model

class Business(NoSQLModel):
    id              = columns.UUID(primary_key = True, default = uuid.uuid4)
    location        = columns.UserDefinedType(Location, required=True)
    title           = columns.Text(required = True)

# Buyer is a PostgreSQL model

class Buyer(SQLModel):
    id              = sqlmodels.UUIDField(primary_key = True, default = uuid.uuid4)
    first_name      = sqlmodels.CharField(blank = False, null = False, max_length = 64)
    last_name       = sqlmodels.CharField(blank = False, null = False, max_length = 64)
